﻿using UnityEngine;
using System.Collections;

public class Enemy : MovingObject {

    public bool isEnemyStrong;
    public bool isEnemyAlt;
    public int attackDamage;
    public AudioClip enemySound1;
    public AudioClip enemySound2;

    private bool skipCurrentMove;
    private Transform player;
    private Transform player2;
    private Transform player3;
    private Animator animator;

	protected override void Start () {
        GameController.Instance.AddEnemyToList(this);
        player = GameObject.FindGameObjectWithTag("Player").transform;
        //player2 = GameObject.FindGameObjectWithTag("Roman 1").transform;
        //player3 = GameObject.FindGameObjectWithTag("Roman 2").transform;
        skipCurrentMove = true;
        animator = GetComponent<Animator>();
        base.Start();
	}

    public void MoveEnemy()
    {
        if(skipCurrentMove)
        {
            if(isEnemyStrong)
            {
                int chanceToMove = Random.Range(1, 4);
                if(chanceToMove > 1)
                {
                    skipCurrentMove = false;
                    return;
                }
            }
            else if (isEnemyAlt)
            {
                int chanceToMove = Random.Range(1, 6);
                if (chanceToMove > 1)
                {
                    skipCurrentMove = false;
                    return;
                }
            }
            else
            {
                skipCurrentMove = false;
                return;
            }
        }

        int xAxis = 0;
        int yAxis = 0;

        float xAxisDistance = Mathf.Abs(player.position.x - transform.position.x);
        float yAxisDistance = Mathf.Abs(player.position.y - transform.position.y);
        //float xAxisDistance2 = Mathf.Abs(player2.position.x - transform.position.x);
        //float yAxisDistance2 = Mathf.Abs(player2.position.y - transform.position.y);

        if (xAxisDistance > yAxisDistance /*|| xAxisDistance2 > yAxisDistance2*/)
        {
            if(player.position.x > transform.position.x /*|| player2.position.x > transform.position.x
                || player3.position.x > transform.position.x*/)
            {
                xAxis = 1;
            }
            else
            {
                xAxis = -1;
            }
        }
        else
        {
            if(player.position.y > transform.position.y /*|| player2.position.y > transform.position.y 
                || player3.position.y > transform.position.y*/)
            {
                yAxis = 1;
            }
            else
            {
                yAxis = -1;
            }
        }

        Move<Player>(xAxis, yAxis);
        skipCurrentMove = true;
    }

    protected override void HandleCollision<T>(T component)
    {
        Player player = component as Player;
        //Roman1 player2 = component as Roman1;
        player.TakeDamage(attackDamage);
        //player2.TakeDamage(attackDamage);
        /*Roman1 roman1 = component as Roman1;
        roman1.TakeDamage(attackDamage);
        Roman2 roman2 = component as Roman2;
        roman2.TakeDamage(attackDamage);*/
        animator.SetTrigger("enemyAttack");
        SoundController.Instance.PlaySingle(enemySound1, enemySound2);
    }
}
