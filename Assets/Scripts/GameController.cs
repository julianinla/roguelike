﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public int playerCurrentHealth = 50;
    public AudioClip gameOverSound;

    private BoardController boardController;
    private List<Enemy> enemies;
    private GameObject levelImage;
    private Text levelText;
    private GameObject startImage;
    private Text titleText;
    private Text startPrompt;
    private bool settingUpGame;
    private GameObject selectScreen;
    private Text selectText;
    private Button firstChoice;
    private Button secondChoice;
    private Text choice1;
    private Text choice2;
    private int secondsUntilLevelStart = 2;
    private int currentLevel = 1;
    private bool startScreenOn;

	void Awake () {
        if (Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
	}
	
    void Start()
    {
        InitializeGame();
    }

    private void InitializeGame()
    {
        settingUpGame = true;

        startImage = GameObject.Find("Start Screen");
        titleText = GameObject.Find("Title").GetComponent<Text>();
        startPrompt = GameObject.Find("Start Prompt").GetComponent<Text>();
        titleText.text = "The Thirteenth Legion";
        startPrompt.text = "Tap Screen to Begin";
        startImage.SetActive(true);
        startScreenOn = true;

		selectScreen = GameObject.Find("Select Screen");
		//firstChoice = Button.name;
		selectText = GameObject.Find("Characters").GetComponent<Text>();
        choice1 = GameObject.Find("Choice 1 Text").GetComponent<Text>();
        choice2 = GameObject.Find("Choice 2 Text").GetComponent<Text>();
        selectText.text = "Select Champion";
        choice1.text = "Centurion";
        choice2.text = "Spearman";

        levelImage = GameObject.Find("Level Image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        levelText.text = "Day " + currentLevel;
        levelImage.SetActive(true);
		
		enemies.Clear();
		boardController.SetupLevel(currentLevel);
		if (currentLevel > 1)
		{
			startScreenOn = false;
			startImage.SetActive(false);
			Invoke("DisableStartScreen", 0);
			Invoke("DisableSelectScreen", 0);
			Invoke("DisableLevelImage", secondsUntilLevelStart);
		}

        //startGame();
    }


    private void DisableStartScreen()
    {
		startImage = GameObject.Find("Start Screen");
        startImage.SetActive(false);
    }

    private void DisableSelectScreen()
    {
		selectScreen = GameObject.Find("Select Screen");
        selectScreen.SetActive(false);
    }

    private void DisableLevelCall()
    {
        Invoke("DisableLevelImage", secondsUntilLevelStart);
    }

    private void DisableLevelImage()
    {
		levelImage = GameObject.Find("Level Image");
        levelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;
    }

    public void startGame()
    {
		Invoke("DisableSelectScreen", 0);
		Invoke("DisableLevelCall", secondsUntilLevelStart);  
    }

    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        InitializeGame();
    }

    /*levelImage = GameObject.Find("Level Image");
            levelText = GameObject.Find("Level Text").GetComponent<Text>();
            levelText.text = "Day " + currentLevel;
            levelImage.SetActive(true);*/

	void Update () {

        if (startScreenOn == true)
        {
            if (Input.touchCount > 0 || Input.GetKeyDown("space"))
            {
                startImage.SetActive(false);
                //Invoke("DisableStartScreen", secondsUntilLevelStart);
                selectScreen.SetActive(true);
                startScreenOn = false;
            }
        }

        if (isPlayerTurn || areEnemiesMoving) {
			return;
		} 
		//else if (isPlayerTurn) {
		//	isPlayerTurn = true;
		//}

        StartCoroutine(MoveEnemies());
	}

    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach(Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }

        areEnemiesMoving = false;
        isPlayerTurn = true;
    }

    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    public void GameOver()
    {
        isPlayerTurn = false;

        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);

        if(currentLevel == 1) {
            levelText.text = "You died after " + currentLevel + " day...";
            levelImage.SetActive(true);
            enabled = false;
        }
        else if (currentLevel == 7)
        {
            levelText.text = "You died after " + currentLevel/7 + " week...";
            levelImage.SetActive(true);
            enabled = false;
        }
        else if ((currentLevel % 7) == 0)
        {
            levelText.text = "You died after " + currentLevel/7 + " weeks...";
            levelImage.SetActive(true);
            enabled = false;
        }
        else 
        {
            levelText.text = "You died after " + currentLevel + " days...";
            levelImage.SetActive(true);
            enabled = false;
        }

    }
}
