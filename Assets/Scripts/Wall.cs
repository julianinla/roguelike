﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

    public int hitPoints = 3;
    public Sprite damagedWallSprite;

    private SpriteRenderer spriteRenderer;

	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
	}

    public void DamageWall(int damageReceived)
    {
        hitPoints -= damageReceived;

        if(hitPoints <= 0)
        {
            gameObject.SetActive(false);
        }
    }
	
}
